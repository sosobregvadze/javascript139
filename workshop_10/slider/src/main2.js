const sliderElements = document.querySelectorAll('.slider');
const sliders = new Map();


// console.log(sliderElements)
const sliderLogic = (sliderElement) => {
    const slider = sliders.get(sliderElement);
    const imagesWrapper = sliderElement.querySelector('div');

    const {size, current} = slider;
    console.log(size, current);
    imagesWrapper.style.left = `${-sliderElement.clientWidth * current}px`;

    slider.current++;
    if (slider.current >= size) {
        slider.current = 0;
    }
};

for (const sliderElement of sliderElements) {
    // slider initialization
    sliders.set(sliderElement, {
        current: 0,
        size: sliderElement.querySelectorAll('img').length
    })

    setInterval(() => sliderLogic(sliderElement), 1000);
}