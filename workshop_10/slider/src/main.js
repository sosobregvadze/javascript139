const sliders = document.querySelectorAll('.slider');
const sliderInfo = {};

const sliderWorker = (slider) => {
    const sliderImages = slider.querySelector('.images-wrapper');
    sliderInfo[slider].current += 1;
    if (sliderInfo[slider].current >= sliderInfo[slider].size) {
        sliderInfo[slider].current = 0;
    }
    const {current} = sliderInfo[slider];

    sliderImages.style.left = `${-sliderImages.clientWidth * current}px`;
}

for (const slider of sliders) { 
    sliderInfo[slider] = {
        size: slider.querySelectorAll('.images-wrapper>img').length,
        current: 0
    }
    setInterval(() => sliderWorker(slider), 2000);
}