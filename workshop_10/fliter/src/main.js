import data from './items.js';

document.querySelector('#search-bar').oninput = (e) => {
    // console.log(e.target.value);

    let dataHTML = '';

    data
        .filter(movie => movie.includes(e.target.value))
        .forEach(movie => {
            dataHTML += `<li>${movie}</li>`;
        });

    document.querySelector('ul').innerHTML = dataHTML;
}

console.log(data);