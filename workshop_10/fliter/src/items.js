const data = [];


for (let i = 0; i < 100; i++) {
    data.push(`movie ${i}`);
}

let dataHTML = '';

data.forEach(movie => {
    dataHTML += `<li>${movie}</li>`
})

document.querySelector('ul').innerHTML = dataHTML;
export default data;