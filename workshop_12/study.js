class Person {
    #age;
    #id;
    static count = 0;

    constructor(name, age) {
        Person.count++;
        this.#id = 25343122341;
        this.name = name;
        this.age = age;
    }

    get age() {
        return this.#age;
    }

    set age(age) {
        if (age < 0 && 150 > age) {
            throw Error('age went out of boundaries');
        }
        this.#age = age;
    }

    static sayHello() {
        console.log('Hello world');
    }
};


class Student extends Person {
    constructor(name, age, gpa) {
        super(name, age);
        this.gpa = gpa;
    }

    attendClass() {
        console.log(`${this.name} is attending a class`);
    }
};

class Lecturer extends Person {
    constructor(name, age, salary) {
        super(name, age);
        this.salary = salary;
    }

    conductLecture() {
        console.log(`${this.name} is conducting a lecture`);
    }
};


const lecturer = new Lecturer('Joe', 45, 7000);
const students = [
    new Student('Marco', 23, 3.5),
    new Student('Marco 1', 23, 3.5),
    new Student('Marco 2', 23, 3.5),
    new Student('Marco 3', 23, 3.5),
    new Student('Marco 4', 23, 3.5),
]

Lecturer.sayHello();
Person.sayHello();
lecturer.age = 25;
console.log(Person.count);

console.log(lecturer.age);
lecturer.conductLecture();

for (const student of students) {
    student.attendClass();
    student.age++;
}