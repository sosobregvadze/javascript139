class Animal {
    constructor(name, age, legs) {
        this.name = name;
        this.age = age;
        this.legs = legs;
        this.sound = null;
    }

    speak() {
        console.log(`${this.name} says: ${this.sound}`);
    }
};

class Dog extends Animal {
    constructor(name, age, legs) {
        super(name, age, legs);
        this.sound = 'Woof Woof';
    }
};

class Cat extends Animal {
    constructor(name, age, legs) {
        super(name, age, legs);
        this.sound = 'Meow';
    }

    jump() {
        console.log(`${this.name} is jumping`);
    }
}

class Bird extends Animal {
    constructor(name, age, legs, featherColor) {
        super(name, age, legs);
        this.featherColor = featherColor;
    }

    speak() {
        console.log()
    }
}


const myPet = new Dog('doggo', 3, 4);
const myPet2 = new Cat('Parr', 5, 4);


myPet2.jump();
myPet.speak();
console.log(myPet);
myPet2.speak();