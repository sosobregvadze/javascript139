class Car {
    constructor(model, brand, price, owner) {
        // method
        this.model = model;  // property | attributes | field
        this.brand = brand;
        this.price = price;
        this.owner = owner;
    }

    describe() {
        // method
        console.log(this.model, this.brand, this.price);
    }

    move() {
        console.log(this.model, 'is moving')
    }

}

const car = new Car("X5", "BMW", 45000); // object / instance of Car class
const car2 = new Car("S3", "Tesla", 45000);

// console.log(car.model);
// console.log(car_2.model);
car.describe();
car2.describe();
car.move();