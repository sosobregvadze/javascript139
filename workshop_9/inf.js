const imgTag = document.querySelector('#dog-image');
const clockTag = document.querySelector('#clock');
const timeSinceTag = document.querySelector('#time-since');
let lastUpdate = new Date();

/*
ღილაკი რომელიც API-დან წამოსულ ფოტოს ჩასვავს img#dog-image თეგში

1. წამოვიღოთ API-დან url
2. ღილაკზე დაჭერის დროს შეიცვალოდ img თეგის url
*/

// ავიღეთ თეგი ჯავასცკიპტში HTML-დან
const generateButton = document.querySelector('#generate');

// დავამატეთ event listener დაჭერაზე
generateButton.onclick = async e => {
    // წამოვიღეთ ლინკი სხვა საიტიდან (API)
    const url = await giveMeDogImageURL();
    console.log(url);
    // ფოტოს თეგში src შევცვალეთ
    imgTag.src = url;

    lastUpdate = new Date();
}

const giveMeDogImageURL = async () => {
    const response = await fetch('https://random.dog/woof.json');
    const data = await response.json();
    return data.url;
};

setInterval(() => {
    const now = new Date();
    const timeSince = Math.floor((now - lastUpdate) / 1000);
    // console.log(timeSince);
    timeSinceTag.innerText = `Last time you clicked the button is ${timeSince} seconds`;
}, 1000)

