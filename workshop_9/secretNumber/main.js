/*

რიცხვის შმოტანა 

ეს რიცხვი შევინახოთ localStorage-ში

ღილაკი რომელიც alert -ით ამოუგდებს ამ შენახულ რიცხვს
წაშლის ღილაკი, რომელიც წაუშლის ამ შენახულ რიცხვს
*/

const numInput = document.querySelector('#secret-input');
const setBtn = document.querySelector('#set');
const showBtn = document.querySelector('#show');
const removeBtn = document.querySelector('#remove');

setBtn.onclick = () => {
    const newSecretNumber = numInput.value;
    window.localStorage.setItem('secretNum', newSecretNumber);
    numInput.value = null;
};

showBtn.onclick = () => {
    const newSecretNumber = window.localStorage.getItem('secretNum');

    alert(`Your secret number is ${newSecretNumber}`);
}


removeBtn.onclick = () => {
    window.localStorage.removeItem('secretNum')
}

