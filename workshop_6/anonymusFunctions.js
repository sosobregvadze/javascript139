// const greet = function() {
//     console.log('greetings')
// }

const greet = (firstName) => {
    console.log(`greetings ${firstName}`);
};

function compare(a, b) {
    return a - b;
}

greet("Leo");


const numbers = [1, 2, -3, -2, 10, 25, 2, 20, 100];

// numbers.sort(compare); // higher order function
// numbers.sort(function(a, b) { return a - b; });
numbers.sort((a, b) => a - b);
console.log(numbers);
