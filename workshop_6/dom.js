const pTag = document.getElementById('text');
const pTags = Array.from(document.getElementsByClassName('some-class'));
const pTags1 = document.getElementsByTagName('p');
console.log(pTags);
const numbers = [5, -7, 3, 4];
numbers.forEach((number) => {
    console.log(number);
});

pTags.forEach(p => {
    p.innerHTML = '<span style="color: gray;">Hello there</span>';
    // p.innerText = "Gamarjoba";
    p.style.color = 'white';
});
// for (let i = 0; i < pTags.length; i++) {
//     pTags[i].style.color = 'white';
// }


console.log(pTags);
console.log(pTags1);
let deg = 90
let colors = 'blue, red, green';
console.log(pTag);

pTag.style.background = `linear-gradient(${deg}deg, red, green, blue)`;
// pTag.style.background = 'linear-gradient(' + String(deg) + 'deg, red, green, blue)';
pTag.style.height = '500px';

// setInterval(() => {
//     console.log('from interval');
//     deg += 1;
//     deg %= 360;
//     pTag.style.background = `linear-gradient(${deg}deg, ${colors})`;
// }, 10);


pTag.onclick = (event) => {
    // console.log(event);
    deg += 10;
    deg %= 360;
    console.log(deg);

    if (deg === 0){
        if (colors === 'blue, red, green'){
            colors = 'red, green, blue';
        } else {
            colors = 'blue, red, green';
        }

    }
    pTag.style.background = `linear-gradient(${deg}deg, ${colors})`;
}

