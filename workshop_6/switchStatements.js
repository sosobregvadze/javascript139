const product = {
    'title': 'Asus ROG 15',
    'price': 3000,
}


if (product.price === 4000) {
    // ...
    console.log('price is 4000 (if)');

} else if (product.price === 3500) {
    // ...
    console.log('price is 3500 (if)');

} else if (product.price === 3000) {
    // ...
    console.log('price is 3000 (if)');

} else {
    // ...
    console.log('default case (if/else)');

}


switch (product.price) {
    case 4000:
        console.log('price is 4000');
        break;
    case 3500:
        console.log('price is 3500');
        break;
    case 3000:
        console.log('price is 3000');
        break;
    default:
        console.log('default case is executed')
        break;
}