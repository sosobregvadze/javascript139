const formElement = document.querySelector('#form');
const formErrors = formElement.querySelector('#errors');

formElement.onsubmit = e => {
    const field1 = formElement.querySelector('#field1');
    const field2 = formElement.querySelector('#field2');
    const errors = [];

    if (field1.value === '' || field1.value === null) {
        errors.push('field 1 should not be empty!');
    }
    if (field2.value === '' || field2.value === null) {
        errors.push('field 2 should not be empty!');
    }

    if (field2.value.search(/^[aeiou][a-z]*[A-Z]$/) === -1) {
        errors.push('field 2 must start with vowel and end with upper case letter');
    }

    if (errors.length !== 0) {
        e.preventDefault();
        formErrors.innerText = errors.join('\n');
    }
};