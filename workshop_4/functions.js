'use strict';
// Global scope
const firstName = "Nika";

function generateText() {
    return "Hello ";
}

function greet(firstName="World") {
    // Function definition
    // Local scope
    // variable shadowing
    // console.log("Hello " + firstName);
    const text = generateText() + firstName;

    return text;
}



function processPayment(amount, card_number) {
    console.log('from procees_payment ' + firstName);
    console.log(card_number + ' was charged ' + String(amount))
}



/*
greet("Nika"); // function invoke; call
greet("Joe");
greet();
greet("Nika");
console.log(firstName);


processPayment(1000, "5043224342");
*/

console.log(greet("Hose")) // "" | 


function shouldSell(value) {
    // if (2000 < value) {
    //     return 'Sell';
    // }

    // return 'Buy';
    return 2000 < value ? 'Sell' : 'Buy';
}

const currentPrice = Number(prompt("Enter current coin value"));
// console.log(shouldSell(20001));
document.querySelector('#should-sell').innerText = shouldSell(currentPrice);
document.querySelector('#user').innerText = greet("User");

