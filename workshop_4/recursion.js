// 0, 1, 1, 2, 3, 5, 8
let fpp = 0;
let fp = 1;
let f;
const n = 10;

for (let i = 2; i < n; i++) {
    f = fp + fpp; // 1 + 1 -> 2 
    fpp = fp; // 1
    fp = f; // 1
}

// console.log('Fibonacci at ' + String(n) + ' Position is: ' + String(f));
// fib (5) -> fib(4) + fib(3)
// fib(1) -> 0
// fib(2) -> 1
// fib(3) -> 0 + 1 

function fib(n) {
    // recursion -> function that calls itself
    if (n === 1) {
        // base / terminal case
        return 0;
    } else if (n === 2) {
        // base / terminal case
        return 1;
    }
    return fib(n - 1) + fib(n - 2);
}

// რატო უჭირს ამის გამოთვლა? / რამდენი ფუნქციის გამოძახებაა?
console.log(fib(11));