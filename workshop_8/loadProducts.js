const productsAPIURL = 'http://167.99.91.187:8000/products/';
const productsWrapperDiv = document.querySelector('#products-wrapper');
const sortSelect = document.querySelector('#sort-by-select');
let products;

const sortByCategory = () => {
    products.sort((a, b) => {
        if (a.category > b.category)
            return 1;
        if (a.category < b.category)
            return -1;
        else return 0;
    });
};

const sortByPrice = () => {
    products.sort((a, b)=> {
        return a.price - b.price;
    });
};

const sortByName = () => {
    products.sort((a, b)=> {
        if (a.name > b.name)
            return 1;
        if (a.name < b.name)
            return -1;
        else return 0;
    });
};

const customSort = key => {
    products.sort((a, b) => {
        if (a[key] > b[key])
            return 1;
        if (a[key] < b[key])
            return -1;
        else return 0;
    })
}

sortSelect.onchange = (e) => {
    const selected = parseInt(e.target.value);

    switch (selected) {
        case 1:
            customSort("price");
            break;
        case 2:
            customSort("name");
            break;
        case 3:
            customSort("category");
            break;
    }
    renderProducts();
}

const fetchData = async () => {
    console.log("Fetching products from API");
    const response = await fetch(productsAPIURL);
    const data = await response.json();
    return data;
}

const renderProducts = () => {
    console.log("Rendering products!");
    productsWrapperDiv.innerHTML = '';
    products.forEach(product => {
        // destucturing
        const {name, price, category} = product;
        const productDiv = document.createElement('div');
        productDiv.innerHTML = (
            `<div class="card">
                <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-body-secondary">${category}</h6>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <p class="card-text text-success">${price}$</p>
                </div>
            </div>`
        );
        productsWrapperDiv.appendChild(productDiv);
    });
}

const insertProducts = async () => {
    products = await fetchData();
    renderProducts();
};


insertProducts();