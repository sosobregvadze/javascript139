
const p1 = new Promise(
    (resolve, reject) => {
        // Producing code
        const num = Math.random() * 100;
        if (num > 50) {
            resolve(num);
        } else {
            reject(num);
        }
    }
);

// consuming code
p1.then(
    (message) => {
        // callbacks
        console.log(message);
    },
    (badMessage) => {
        console.log("Sorry lost connection!");
        console.log(badMessage);
    }
);