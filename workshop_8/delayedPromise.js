const p = new Promise(
    (resolve, reject) => {
        // Producing code
        const getRandomNumber = () => parseInt(Math.random() * 100);
        setTimeout(() => {
            const randomNumber = getRandomNumber();
            if (randomNumber > 50) {
                resolve(randomNumber); // success 
            } else {
                reject(randomNumber); // failure
            }
        }, 3000);
    }
);


// Consumig code
p.then(
    (randomNumber) => {
        // callback for success
        console.log(`Resolved ${randomNumber}`);
    },
    (randomNumber) => {
        // callback for failure
        console.log(`Rejected ${randomNumber}`);
    }
)