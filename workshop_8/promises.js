const productsAPIURL = 'http://167.99.91.187:8000/products/';

const fetchDataAsync = async () => {
    console.log('Fetching [Async] ...');
    setTimeout(async () => {
        const response = await fetch(productsAPIURL);
        const data = await response.json();
        console.log(data);
    }, 3000);

}

// fetchDataAsync();
// fetchDataAsync();

const sendRequest = (asyncronous = false) => {
    // AJAX
    let req = new XMLHttpRequest();

    req.open('GET', productsAPIURL, asyncronous);
    console.log('Sending request')
    req.onload = () => {
        console.log('>>> Got Response! <<<');
    };
    req.send();
}

sendRequest();
console.log('code continued');
sendRequest();

// p2.then(
//     console.log,
//     console.log
// );