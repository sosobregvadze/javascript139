
const person = {
    'name': 'John',
    'lastName': 'Doe',
    'pet': {
        'name': 'Barley',
        'age': 5,
        'walk': function() {
            return `${this.name} woof woof`;
        }
    },
    'walk': function() {
        // console.log(this.name + ' is walking');
        console.log(`${this.name} is walking`);
    },
    'describe': function() {
        console.log(`${this.name} ${this.lastName} has dog named ${this.pet.name}`);
    }
};

// person.walk();
// person.describe();
// console.log(person.pet.walk());
// console.log(person.pet.name);


function test() {
    console.log('hello');
}


const products = [
    {
        name: 'Laptop',
        price: 1750
    },
    {
        name: 'car',
        price: 9000
    },
    {
        name: 'product 1',
        price: 500
    },
    {
        name: 'product 2',
        price: 20
    },
    {
        name: 'product 3',
        price: 32
    },
    {
        name: 'product 4',
        price: 78
    },
];

console.log(products[3]);
const productsDiv = document.querySelector('#products');
console.log(productsDiv.onclick);
let finalText = '';

for (let i = 0; i < products.length; i++) {
    const product = products[i];
    const productText = `${product.name} costs ${product.price} USD`;
    // const productDiv = document.createElement('div');
    // productDiv.innerText = `${product.name} costs ${product.price} USD`;
    // productsDiv.appendChild(productDiv);

    // 2 path
    finalText += `<div>${productText}</div>`;
}
productsDiv.innerHTML = finalText;


// setInterval(person.walk, 2000);

setTimeout(test, 3000);