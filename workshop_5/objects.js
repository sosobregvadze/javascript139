// objects
const car = {
    name: "fiat",
    model: "500",
    weight: 850,
    color: "white"
}

const car2 = {
    name: "mecredes",
    model: "cls",
    weight: 1000,
    color: "black"
}

const car3 = {
    name: "Tesla",
    model: "model x",
    weight: 1000,
    color: "red"
}

// accsesing single value
console.log(car.name);
console.log(car['name']);
console.log(car['model']);

// assigning new value (key/value pair)
car.owner = "Joe";

console.log(car.owner);

// non exsitent keys
console.log(car['engine']); // undefined

console.log(car);
delete car.owner;

// change
car2.name = "BMW";
console.log(car2);
// console.log(car3);

