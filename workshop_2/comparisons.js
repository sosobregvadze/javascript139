// Comparison operators
let x = 5;
let y = 5;

// equality
console.log(x == y);
console.log(x === y);

// inequality
console.log(x != y);
console.log(x !== y);

console.log(x > y); // false
console.log(x >= y); // true
console.log(x < y); // false
console.log(x <= y); // true



