import CarCard from './components/carCard.js';
import generateCars from './data.js';

/*
დაამიპლემენტეთ renderCars ფუნქცია

HTML-ში მოცემულია ორი ელემენტი:
    * div#cars-wrapper          [ანუ div, რომელსაც id აქვს cars-wrapper]
    * button#cars-render-button [ანუ button, რომელსაც id აქვს cars-render-button]

გამზადებულია 2 ფუნქცია:
    * CarCard
        - აბრუნებს HTML სტრინგს, რომელიც 1 car object -ის დასარენდერებლად
    * generateCars
        - აბრუნებს car object -ების array -ს
    
დავალება:

შექმენით 2 ცვლადი, რომელშიც შეინახავთ 2 მოცემულ HTML -ის თეგს
renderCars -ში generateCars ფუნქციის გამოყენებით დააგენერირეთ array და შეინახეთ cars ცვლადში.
შემდეგ თითოეული ელემენტისთვის გამოიძახეთ CarCard ფუნქცია და div#cars-wrapper -ში ჩასვით
თითოეული ელემენტის HTML სტრინგი

button#cars - ზე დაამატეთ onclick event -ი რომელიც დაკიკების დროს გამოიძახებს renderCars ფუნქციას
*/

const carsWrapper = document.querySelector('#cars-wrapper');
const carsRenderButton = document.querySelector('#cars-render-button');

// definition
const renderCars = () => {
    // function body
    const cars = generateCars();
    carsWrapper.innerHTML = "";

    cars.forEach(car => {
        carsWrapper.appendChild(CarCard(car));
    });
};

carsRenderButton.onclick = renderCars;

