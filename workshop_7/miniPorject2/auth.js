import users from "./data.js";

export const checkLogin = (username, password) => {
    for (let i = 0; i < users.length; i++) {
        const user = users[i];
        if (user.username == username && user.password == password)
            return user;
    }
    return null;
}