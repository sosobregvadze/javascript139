import { checkLogin } from './auth.js';

// HTML Elements LoginView
const loginView = document.getElementById('login-view');
const loginViewElements = {
    usernameInput: loginView.querySelector('#username-input'),
    passwordInput: loginView.querySelector('#password-input'),
    loginForm: loginView.querySelector('#login-form')
};

// HTML Element Account View
const accountView = document.getElementById('account-view');
const accountViewElements = {
    usernameDisplay: accountView.querySelector('#username-display'),
    bioDisplay: accountView.querySelector('#user-bio-display'),
    balanceDisplay: accountView.querySelector('#user-balance-display'),
};

// Event listeners
loginViewElements.loginForm.onsubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();

    // destructurig object
    const { usernameInput, passwordInput } = loginViewElements;
    const user = checkLogin(usernameInput.value, passwordInput.value);
    if (!user) {
        usernameInput.classList.add('is-invalid');
        passwordInput.classList.add('is-invalid');
        return;
    }

    loginView.hidden = true;
    accountView.hidden = false;

    const { usernameDisplay, bioDisplay, balanceDisplay } = accountViewElements;
    usernameDisplay.innerText = user.username;
    bioDisplay.innerText = user.bio;
    balanceDisplay.innerText = user.balance;
};;