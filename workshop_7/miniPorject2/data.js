const users = [
    {
        username: 'Helen',
        bio: "Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music.",
        balance: 10000,
        password: 'test'
    }
];

export default users;